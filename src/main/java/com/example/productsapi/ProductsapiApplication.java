package com.example.productsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class ProductsapiApplication {

  public static void main(String[] args) {
    SpringApplication.run(ProductsapiApplication.class, args);
  }

  @GetMapping("/getAll")
  public String[] getProducts(){
    return new String[]{"product 1", "product 2", "product 3"};
  }

}
